package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

var (
	expirydelta time.Duration
)

type CertErrors map[string][]error

func main() {
	var hostname = flag.String("hostname", "", "hostname and port for connection")
	var checkfile = flag.String("checkfile", "hosts.yaml", "file that contains all hosts to check")
	var expirystring = flag.String("expire", "168h", "delta between expiry")
	flag.Parse()

	var err error
	expirydelta, err = time.ParseDuration(*expirystring)
	if err != nil {
		fmt.Printf("invalid expiry string: %v\n", err)
		os.Exit(1)
	}

	var hosts []string

	// append the single hostfile, if not empty
	if *hostname != "" {
		hosts = append(hosts, *hostname)
	}

	// read hosts from YAML file
	yamlfile, err := ioutil.ReadFile(*checkfile)
	if err != nil && !os.IsNotExist(err) {
		fmt.Printf("failed to read hosts file: %v\n", err)
		os.Exit(1)
	}

	if err == nil {
		err = yaml.Unmarshal(yamlfile, &hosts)
		if err != nil {
			fmt.Printf("failed to read hosts file: %v\n", err)
			os.Exit(1)
		}
	}

	sort.Strings(hosts)

	for _, host := range hosts {
		url := host + ":443"

		conn, err := tls.Dial("tcp", url, nil)
		if err != nil {
			fmt.Printf("%s\n", host)
			fmt.Printf(" - %s\n", err)
			continue
		}
		defer conn.Close()

		errs := validateChain(conn.ConnectionState(), host)
		if len(errs) > 0 {
			fmt.Printf("%s\n", host)
			for p, err := range errs {
				fmt.Printf(" - %s\n", p)
				for _, e := range err {
					fmt.Printf("   - %s\n", e)
				}
			}
			fmt.Println()
		}
	}
}

func validateChain(state tls.ConnectionState, requestedHostname string) CertErrors {
	errors := make(map[string][]error)

	for _, chain := range state.VerifiedChains {
		for pos, cert := range chain {
			errs := validateCertificate(pos, cert, requestedHostname)
			if len(errs) > 0 {
				errors[fmt.Sprintf("#%d -  %s", pos, cert.Subject.CommonName)] = errs
			}
		}
	}

	return errors
}

func validateCertificate(chainPos int, cert *x509.Certificate, requestedHostname string) []error {
	var errors []error

	//fmt.Printf("%s\n", cert.Subject.CommonName)

	// make sure the hostname is listed in the SAN names
	// this only applies to the first certificate in the chain
	if chainPos == 0 {
		//fmt.Printf("\tDNS names:  %s\n", strings.Join(cert.DNSNames, ","))
		if !contains(cert.DNSNames, requestedHostname) {
			errors = append(errors,
				fmt.Errorf("SAN error: hostname not in SAN field: %s", strings.Join(cert.DNSNames, ",")))
		}
	}

	if cert.NotBefore.After(time.Now().UTC()) {
		errors = append(errors,
			fmt.Errorf("Date error: certificate is not yet valid: %s\n", cert.NotBefore))
	}

	if cert.NotAfter.Before(time.Now().UTC()) {
		errors = append(errors,
			fmt.Errorf("Date error: certificate is no longer valid: %s\n", cert.NotAfter))
	}

	tdiff := time.Now().UTC().Add(expirydelta)
	if tdiff.After(cert.NotAfter) {
		errors = append(errors,
			fmt.Errorf("Date error: certificate is about to expiry: %s\n", cert.NotAfter))
	}

	return errors
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}
